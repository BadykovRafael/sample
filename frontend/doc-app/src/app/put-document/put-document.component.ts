import {Component, OnInit} from '@angular/core';
import {DocumentModel} from "../document.model";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Component({
  selector: 'dc-put-document',
  templateUrl: './put-document.component.html',
  styleUrls: ['./put-document.component.less']
})
export class PutDocumentComponent implements OnInit {
  public document = new DocumentModel();

  constructor(private httpService: HttpClient) {
  }

  ngOnInit() {
  }

  putDocument(event: DocumentModel) {
    const url = environment.url + "/document";
    this.httpService.put(url, event).subscribe(() => {
      alert("Put successful");
      this.document = new DocumentModel();
    });

  }
}
