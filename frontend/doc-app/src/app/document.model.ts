export class DocumentModel {
  id: number;
  name: string;
  date: Date;
}
