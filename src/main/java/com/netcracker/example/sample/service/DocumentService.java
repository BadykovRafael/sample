package com.netcracker.example.sample.service;

import com.netcracker.example.sample.domain.Document;
import com.netcracker.example.sample.repository.DocumentRepository;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {

    private DocumentRepository repository;

    public DocumentService(DocumentRepository repository) {
        this.repository = repository;
    }

    public Document createDocument(Document request) {
        Document savedDocument = repository.save(request);
        return savedDocument;
    }

    public Document findById(Long id) throws Exception {
        return repository.findById(id).orElseThrow(() -> new Exception("Can't find document with id" + id));
    }
}
